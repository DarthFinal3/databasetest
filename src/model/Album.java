package model;

public class Album {
	
	private Integer id;
	private String title;
	private Integer artistId;
	
	
	
	public Album(Integer id, String title, Integer artistId) {
		super();
		this.id = id;
		this.title = title;
		this.artistId = artistId;
	}
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the artistId
	 */
	public Integer getArtistId() {
		return artistId;
	}
	/**
	 * @param artistId the artistId to set
	 */
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

}
