package model;
/**
 * 
 * @author mircopalese emiliocimino francescocarallo
 * 
 * this class represent an abstraction of album's track
 *
 */
public class Track {
	
	private Integer id;
	private String name;
	private Integer albumId;
	private String genre;
	private Integer milliseconds;
	
	
	public Track(Integer trackId, String name, Integer albumId, String genre, Integer milliseconds) {
		super();
		this.id = trackId;
		this.name = name;
		this.albumId = albumId;
		this.genre = genre;
		this.milliseconds = milliseconds;
	}

	/**
	 * @return the trackId
	 */
	public Integer getTrackId() {
		return id;
	}

	/**
	 * @param trackId the trackId to set
	 */
	public void setTrackId(Integer trackId) {
		this.id = trackId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the albumId
	 */
	public Integer getAlbumId() {
		return albumId;
	}

	/**
	 * @param albumId the albumId to set
	 */
	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	/**
	 * @return the genreId
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genreId the genreId to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * @return the milliseconds
	 */
	public Integer getMilliseconds() {
		return milliseconds;
	}

	/**
	 * @param milliseconds the milliseconds to set
	 */
	public void setMilliseconds(Integer milliseconds) {
		this.milliseconds = milliseconds;
	}



}
