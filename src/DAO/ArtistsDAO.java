package DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import DBConnection.DBConnection;
import DBConnection.TableRecord;
import model.Artist;

public class ArtistsDAO {
	
	private static DBConnection conn;
	private static ArtistsDAO instance; /** for singleton */
	
	
	/*
	 * SINGLETON
	 */
	
	public static ArtistsDAO getInstance(){
		if(conn == null)
			conn = DBConnection.getInstance();
		if(instance == null)
			instance = new ArtistsDAO();
		return instance;
	}
	
	
	
	public ArrayList<Artist> getAllartists() throws SQLException {
		
		Artist artist = null;
		ArrayList<Artist> artistArr = new ArrayList<Artist>();
		String query = "SELECT * FROM artists";
		
		ArrayList<TableRecord> result = conn.query(query);
		Iterator<TableRecord> i = result.iterator();
		
		 while(i.hasNext()){
			TableRecord record = i.next();
			
			artist = new Artist (
					Integer.parseInt(record.get("artist_id")),
					record.get("name")
	        );
			
			artistArr.add(artist);
		 }
        return artistArr;
	}
	
	public int addArtist(String name)
	{
		int result;
		String q= "INSERT INTO artists (name) VALUES ('" + name + "')";
		result=conn.insert(q);
		return result;
	}
	
	public int deleteArtist(String id)
	{
		int result;
		String query= "DELETE FROM artists WHERE artist_id = ('" + id + "')";
		if ((result=conn.update(query)) == 0)
			System.out.println("ERRORE");
		
		return result;
	}

}
