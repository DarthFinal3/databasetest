package DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import DBConnection.DBConnection;
import DBConnection.TableRecord;
import model.Album;
import model.Track;




public class AlbumsDAO {
	
	
	
	private static DBConnection conn;
	private static AlbumsDAO instance; /** for singleton */
	
	
	/*
	 * SINGLETON
	 */
	
	public static AlbumsDAO getInstance(){
		if(conn == null)
			conn = DBConnection.getInstance();
		if(instance == null)
			instance = new AlbumsDAO();
		return instance;
	}
	
	
	public ArrayList<Album> getAllAlbums() throws SQLException {
		
		Album album = null;
		ArrayList<Album> albumArr = new ArrayList<Album>();
		String query = "SELECT * FROM albums";
		
		ArrayList<TableRecord> result = conn.query(query);
		Iterator<TableRecord> i = result.iterator();
		
		 while(i.hasNext()){
			TableRecord record = i.next();
			
			System.out.println(record.get("album_id"));
			album = new Album (
					Integer.parseInt(record.get("album_id")),
					record.get("title"),
	        		Integer.parseInt(record.get("artist_id"))
	        );
			
			albumArr.add(album);
			
		 }
        return albumArr;
	}
	
public ArrayList<Album> getAllAlbumsByArtistName(String name) throws SQLException{
		
		Album album = null;
		ArrayList<Album> albumArr = new ArrayList<Album>();
		String query = "SELECT albums.*\n" + 
				"FROM artists, albums\n" + 
				"WHERE albums.artist_id = artists.artist_id and artists.name ='" + name +"'";
		
		ArrayList<TableRecord> result = conn.query(query);
		Iterator<TableRecord> i = result.iterator();
		
		 while(i.hasNext()){
			TableRecord record = i.next();
			
			album = new Album (
					Integer.parseInt(record.get("album_id")),
					record.get("title"),
					Integer.parseInt(record.get("artist_id"))
	        );
			
			albumArr.add(album);
		 }
        return albumArr;	
        
	}

}
