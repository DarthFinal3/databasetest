package DAO;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import DBConnection.DBConnection;
import DBConnection.TableRecord;
import model.Track;

public class TracksDAO {
	
	private static DBConnection conn;
	private static TracksDAO instance; /** for singleton */
	
	
	/*
	 * SINGLETON
	 */
	
	public static TracksDAO getInstance(){
		if(conn == null)
			conn = DBConnection.getInstance();
		if(instance == null)
			instance = new TracksDAO();
		return instance;
	}
	
	
	public ArrayList<Track> getAllTracks() throws SQLException {
			
			Track track = null;
			ArrayList<Track> trackArr = new ArrayList<Track>();
			String query = "SELECT * FROM tracks";
			
			ArrayList<TableRecord> result = conn.query(query);
			Iterator<TableRecord> i = result.iterator();
			
			 while(i.hasNext()){
				TableRecord record = i.next();
				
				track = new Track (
						Integer.parseInt(record.get("track_id")),
						record.get("name"),
						Integer.parseInt(record.get("album_id")),
						record.get("genre"),
						Integer.parseInt(record.get("milliseconds"))
		        );
				
				trackArr.add(track);
			 }
	        return trackArr;
		}
	
	/*Ottenere tutte le tracce dato un artista*/
	
	public ArrayList<Track> getAllTracksByArtistName(String name) throws SQLException{
		
		Track track = null;
		ArrayList<Track> trackArr = new ArrayList<Track>();
		String query = "SELECT tracks.*\n" + 
				"FROM tracks, artists, albums\n" + 
				"WHERE tracks.album_id = albums.album_id and albums.artist_id = artists.artist_id and artists.name ='" + name +"'";
		
		ArrayList<TableRecord> result = conn.query(query);
		Iterator<TableRecord> i = result.iterator();
		
		 while(i.hasNext()){
			TableRecord record = i.next();
			
			track = new Track (
					Integer.parseInt(record.get("track_id")),
					record.get("name"),
					Integer.parseInt(record.get("album_id")),
					record.get("genre"),
					Integer.parseInt(record.get("milliseconds"))
	        );
			
			trackArr.add(track);
		 }
        return trackArr;
		
	}
	
public ArrayList<Track> getAllTracksByAlbumName(String name) throws SQLException{
		
		Track track = null;
		ArrayList<Track> trackArr = new ArrayList<Track>();
		String query = "SELECT tracks.*\n" + 
				"FROM tracks, albums\n" + 
				"WHERE tracks.album_id = albums.album_id and albums.title ='" + name +"'";
		
		ArrayList<TableRecord> result = conn.query(query);
		Iterator<TableRecord> i = result.iterator();
		
		 while(i.hasNext()){
			TableRecord record = i.next();
			
			track = new Track (
					Integer.parseInt(record.get("track_id")),
					record.get("name"),
					Integer.parseInt(record.get("album_id")),
					record.get("genre"),
					Integer.parseInt(record.get("milliseconds"))
	        );
			
			trackArr.add(track);
		 }
        return trackArr;
		
	}
	
	
	public ArrayList<Track> getTrackById(String id) throws SQLException{
		
		Track track = null;
		ArrayList<Track> trackArr = new ArrayList<Track>();
		String query = "SELECT tracks.* FROM tracks WHERE tracks.track_id = " + id;
		
		ArrayList<TableRecord> result = conn.query(query);
		Iterator<TableRecord> i = result.iterator();
		
		 while(i.hasNext()){
			TableRecord record = i.next();
			
			track = new Track (
					Integer.parseInt(record.get("track_id")),
					record.get("name"),
					Integer.parseInt(record.get("album_id")),
					record.get("genre"),
					Integer.parseInt(record.get("milliseconds"))
	        );
			
			trackArr.add(track);
		 }
        return trackArr;
	}
	
	
	public void UpdateTrackName(Track t) throws SQLException {
		
		String query= "UPDATE tracks SET name = '" + t.getName() +"' WHERE tracks.track_id=" + t.getTrackId();
		conn.update(query);
		
	}
	
	


}
