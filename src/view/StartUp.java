package view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import DAO.AlbumsDAO;
import DAO.ArtistsDAO;
import DAO.TracksDAO;
import model.Album;
import model.Artist;
import model.Track;

public class StartUp {

	public static void main(String[] args) throws SQLException {
		
		ArrayList<Album> albums = AlbumsDAO.getInstance().getAllAlbums();
		
		ArrayList<Track> tracks  = TracksDAO.getInstance().getAllTracks();
		
		ArrayList<Artist> artists = ArtistsDAO.getInstance().getAllartists();
		
		Scanner scanner = new Scanner(System.in);
		String option="";

		
		PrintArtitst(artists);
//		PrintAlbums(albums);
//		PrintTracks(tracks);
			
		
		while (true){
			albums.clear();
			artists.clear();
			tracks.clear();
			option="";
			System.out.println(" ");
			System.out.println("Select a new opeation:");
			System.out.println("A: Get all tracks from artist name");
			System.out.println("B: Rename a track");
			System.out.println("C: Insert a new artist");
			System.out.println("D: Delete an artist");
			System.out.println("E: Get all albums from artist name");
			System.out.println("F: Get all tracks from album name");
			System.out.println("END: Terminate");
			
			
			option=scanner.nextLine();
			
			switch (option) {
				case "A":
					getTrackByArtistName(tracks);
					break;
				case "B":
					RenameTrack();
					break;
				case "C":
					newArtist();
					break;
				case "D":
					deleteArtist();
					break;
				case "E":
					getAlbumByArtistName(albums);
					break;
				case "F":
					getTrackByAlbumName(tracks);
					break;
				case"END":
					return;
			
			}
			
		}
		
		
	}
	
	public static void deleteArtist()
	{
		System.out.println("Please, insert an artist id:");
		Scanner scan = new Scanner(System.in);
		String id = scan.nextLine();
		int a;
		if((a =  ArtistsDAO.getInstance().deleteArtist(id)) > 0)
		{
			System.out.println("Artist deleted");
		}
		System.out.println("Rows affected: " + a);
	}
	
	public static void newArtist()
	{
		System.out.println("Please, insert an artist name:");
		Scanner scan = new Scanner(System.in);
		String name = scan.nextLine();
		if(ArtistsDAO.getInstance().addArtist(name) > 0)
		{
			System.out.println("Artist added");
		}
		
		
	}
	
	
	public static void getTrackByArtistName (ArrayList<Track> tracks) throws SQLException{
		System.out.println("Please, insert an artist name:");
		Scanner scan2 = new Scanner(System.in);
		String name = scan2.nextLine();
		tracks= TracksDAO.getInstance().getAllTracksByArtistName(name);
		if (tracks.size() > 0) {
			PrintTracks(tracks);
		}
		else if(tracks.size() == 0) {
			System.out.println("Sorry, there are not tracks from this artist! Try with another one!");
		}
		
	}
	
	public static void getTrackByAlbumName (ArrayList<Track> tracks) throws SQLException{
		System.out.println("Please, insert an album name:");
		Scanner scan2 = new Scanner(System.in);
		String name = scan2.nextLine();
		tracks= TracksDAO.getInstance().getAllTracksByAlbumName(name);
		if (tracks.size() > 0) {
			PrintTracks(tracks);
		}
		else if(tracks.size() == 0) {
			System.out.println("Sorry, there are not tracks from this album! Try with another one!");
		}
		
	}
	
	public static void getAlbumByArtistName (ArrayList<Album> albums) throws SQLException{
		System.out.println("Please, insert an artist name:");
		Scanner scan2 = new Scanner(System.in);
		String name = scan2.nextLine();
		albums= AlbumsDAO.getInstance().getAllAlbumsByArtistName(name);
		if (albums.size() > 0) {
			PrintAlbums(albums);
		}
		else if(albums.size() == 0) {
			System.out.println("Sorry, there are not albums from this artist! Try with another one!");
		}
		
	}
	
	public static void RenameTrack () throws SQLException {
		System.out.println("Please, insert track id:");
		Scanner scan = new Scanner(System.in);
		String id = scan.nextLine();
		Track track= TracksDAO.getInstance().getTrackById(id).get(0);
		if (track.equals(null)) {
			System.out.println("Sorry, no track was found");
		}
		else
		{
			System.out.println("Track to be modified: " + track.getName());
			System.out.println("Insert the new name:");
			track.setName(scan.nextLine());
			TracksDAO.getInstance().UpdateTrackName(track);
			System.out.println("Track name updated");
		}
		
		
	}
	
	
	
	public static void PrintTracks(ArrayList<Track> tracks)
	{
		System.out.println("********** TRACKS **********");
		for(int i = 0; i < tracks.size(); i++) {
			System.out.println("track: "+ tracks.get(i).getName() + " id:" + tracks.get(i).getTrackId());
			//System.out.println("genre "+ tracks.get(i).getGenre());
			//System.out.println("duration(milliseconds) "+ tracks.get(i).getMilliseconds());
		}
	}
	
	public static void PrintArtitst(ArrayList<Artist> artists) {
		System.out.println("********** ARTISTS **********");
		for(int i = 0; i < artists.size(); i++) {
			System.out.println("name: "+ artists.get(i).getName());
		}
	}
	
	
	public static void PrintAlbums(ArrayList<Album> albums) {
		System.out.println("********** ALBUMS **********");
		for(int i = 0; i < albums.size(); i++) {
			System.out.println("album: "+ albums.get(i).getTitle());
		}
	}

}
